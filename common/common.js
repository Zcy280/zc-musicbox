const utils = {};
var audio = uni.createInnerAudioContext()
var isPause = false   //是否在暂停状态
var previousSrc = ''   //上一个音频的地址，如果和现在的播放地址一样就进入暂停

utils.playAudios = function (src, volume) {
	if(isPause){    //在暂停状态
		if(previousSrc == src){
			audio.play()
		}else{
			audio.stop()
			audio.src = src
			previousSrc = src
			audio.onCanplay(()=> {
				audio.play()
			})	
		}
	}else{ //不在暂停状态
		if(previousSrc == src){
			audio.pause()
			isPause = true
		}else{
			audio.stop()
			audio.src = src
			previousSrc = src
			audio.onCanplay(()=> {
				audio.play()
			})
		}
	}
}
utils.stopAudios = function () {
	audio.stop()
}
utils.pauseAudios = function () {
	audio.pause()
}

audio.onPlay(() => {  //播放监听事件
  console.log('开始播放');
   isPause = false
});
audio.onPause(() => {//暂停监听
  console.log('暂停');
  isPause = true
});

audio.onEnded(() => {// 自然播放结束监听也需要更改isPause 开关状态
  console.log('音频自然播放结束事件');
  isPause = true
});

export default utils