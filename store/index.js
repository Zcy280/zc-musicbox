import { createStore } from 'vuex'
const store = createStore({
	state:{//存放状态
		"coverurl":'',
		'PlayBox':uni.createInnerAudioContext(),
		'Playstatus':0,
		'Songid':'',
		'Audiolrc':[],
		'lrcindex':0,
	},
	    mutations:{
	        // 这里第一个形参state就是仓库state，是可以访问到state里的msg的值，即 可以修改state
	        // 第二个形参params是actions中传过来的数据
	        CoverChange(state,url){
				state.coverurl=url
	        }
	    },
})

export default store